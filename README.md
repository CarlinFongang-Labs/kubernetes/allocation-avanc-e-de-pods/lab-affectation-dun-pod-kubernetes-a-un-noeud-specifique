# Lab - Affectation dun pod Kubernetes à un nœud spécifique

------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


## Objectifs

1. Configurez le pod `auth-gateway` pour qu'il s'exécute uniquement sur `k8s-worker2`

2. Configurez les pods de réplication du déploiement « auth-data » pour qu'ils s'exécutent uniquement sur « k8s-worker2 »


# Contexte 

Vous travaillez pour BeeHive, une entreprise qui assure des expéditions régulières d'abeilles aux clients. La société dispose de quelques pods exécutés dans son cluster Kubernetes qui dépendent de services spéciaux existant en dehors du cluster. Ces services sont très sensibles et l'équipe de sécurité a demandé qu'ils ne soient exposés qu'à certains segments du réseau.

Malheureusement, seul le k8s-worker2nœud existe dans le segment de réseau partagé par ces services. Cela signifie que seuls les pods du k8s-worker2nœud pourront accéder à ces services externes sensibles, et que les pods du k8s-worker1ou k8s-controldes nœuds ne pourront pas y accéder.

Votre tâche consiste à reconfigurer le auth-gatewaypod et les auth-datapods de réplique du déploiement afin qu'ils s'exécutent toujours sur le k8s-worker2nœud.

>![Alt text](img/image.png)

# Application

## Laboratoire : Planification de Pods sur un Nœud Spécifique

### Étape 1 : Connexion au Serveur de Laboratoire

Connectez-vous au serveur de laboratoire à l'aide des informations d'identification fournies :

```sh
ssh user@PUBLIC_IP_ADDRESS_CONTROL_NODE
```

Une fois connecté, nous allons afficher tous les noeuds du cluster et choisir le noeud avec lequel nous allons poursuivre 

```sh
kubectl get nodes
```
>![Alt text](img/image-1.png)
*Liste des nodes du cluster*

### Étape 2 : Configuration du Pod `auth-gateway` pour qu'il s'exécute uniquement sur `k8s-worker2`

1. Attachez une étiquette à `k8s-worker2` :

```sh
kubectl label nodes k8s-worker2 external-auth-services=true
```

>![Alt text](img/image-2.png)
*Label attribué au node*

2. Ouvrez le fichier `auth-gateway.yml` :

```sh
nano auth-gateway.yml
```

3. Ajoutez un `nodeSelector` au descripteur du pod `auth-gateway` :

```yaml
...
spec:
    nodeSelector:
    external-auth-services: "true"
...
```

le résultat sera : 

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: auth-gateway
  namespace: beebox-auth
spec:
  containers:
  - name: nginx
    image: nginx:1.19.1
    ports:
    - containerPort: 80
  nodeSelector:
    external-auth-services: "true"
```

4. Enregistrez et quittez le fichier

5. Supprimez et recréez le pod :

```sh
kubectl delete pod auth-gateway -n beebox-auth
kubectl create -f auth-gateway.yml
```

>![Alt text](img/image-3.png)
*Pod auth-gateway crée*

6. Vérifiez que le pod est planifié sur le nœud `k8s-worker2` :

```sh
kubectl get pod auth-gateway -n beebox-auth -o wide
```

>![Alt text](img/image-4.png)
*Le pod auth-gateway est bien attribué au worker2*


### Étape 3 : Configuration des Pods de Réplication `auth-data` pour qu'ils s'exécutent uniquement sur `k8s-worker2`

1. Ouvrez le fichier `auth-data.yml` :

```sh
nano auth-data.yml
```

2. Ajoutez un `nodeSelector` au modèle de pod dans la spécification de déploiement (ce sera le deuxième `spec` du fichier) :

   ```yaml
   ...
   spec:
     ...
     template:
       ...
       spec:
         nodeSelector:
           external-auth-services: "true"
       ...
   ...
   ```

l'on obtiens : 

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: auth-data
  namespace: beebox-auth
spec:
  replicas: 3
  selector:
    matchLabels:
      app: auth-data
  template:
    metadata:
      labels:
        app: auth-data
    spec:
      containers:
      - name: nginx
        image: nginx:1.19.1
        ports:
        - containerPort: 80
      nodeSelector:
        external-auth-services: "true"
```

3. Enregistrez et quittez le fichier

4. Mettez à jour le déploiement :

```sh
kubectl apply -f auth-data.yml
```

>![Alt text](img/image-5.png)
*Deployment mis à jour*

5. Vérifiez que les réplicas du déploiement s'exécutent tous sur `k8s-worker2` :

```sh
kubectl get pods -n beebox-auth -o wide
```

>![Alt text](img/image-6.png)
*Les replicas s'exécute bien sur le worker2*